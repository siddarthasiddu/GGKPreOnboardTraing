﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    class Queue<T>
    {
        Node<T> front = null;
        Node<T> rear = null;
        int _size = 0;
        public void EnQueue(T data)
        {
            if (front == null)
            {
                front = new Node<T>(data);
                rear = front;
            }
            else
            {
                Node<T> temp = new Node<T>(data);
                front.next = temp;
                front = temp;
            }
            _size++;
        }
        public T DeQueue()
        {
            if(rear==null)
            {
                throw new Exception("empty queue");
            }
            Node<T> temp = rear;
            rear = rear.next;
            temp.next = null;
            _size--;
            return temp.data;
        }
        public int size
        {
            get
            {
                return _size; 
            }
        }
        public override string ToString()
        {
            StringBuilder str = new StringBuilder("[]");
            Node<T> curr = rear.next;
            str.Insert(1, rear.data+",");
            while (curr != null)
            {
                str.Insert(str.Length - 1, curr.data + ",");
                curr = curr.next;
            }
            str.Remove(str.Length - 2, 1);
            return str.ToString();
        }
    }
}
