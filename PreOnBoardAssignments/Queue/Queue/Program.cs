﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> stack = new Queue<int>();
            Console.WriteLine("Enter 1 for enQueue \nEnter 2 for deQueue\nEnter 3 for current size of queue\nEnter 4 to print queue\nEnter 5 to stop");
            outer : while(true)
            {
                string[] arr = Console.ReadLine().Split(' ');
                if (int.Parse(arr[0]) == 5) break;
                switch (int.Parse(arr[0]))
                {
                    case 1: stack.EnQueue(int.Parse(arr[1])); break;
                    case 2: Console.WriteLine("poped = " + stack.DeQueue()); break;
                    case 3: Console.WriteLine("size = " + stack.size); break;
                    case 4: Console.WriteLine(stack); break;
                    default:
                        Console.WriteLine("you entered wrong choice");
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}
