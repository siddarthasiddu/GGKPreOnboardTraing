﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTree
{   
    class Node
    {
        private int _data;
        private Node _left;
        private Node _right;

        public Node(int data)
        {
            _data = data;
        }

        public int Data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }

        public Node LeftNode
        {
            get
            {
                return _left;
            }
            set
            {
                _left = value;
            }
        }
        
        public Node RightNode
        {
            get
            {
                return _right;
            }
            set
            {
                _right = value;
            }
        }

    }
    class BSTree
    {
        private Node _root;

        void Insert(int data)
        {
            if (_root == null)
            {
                _root = new Node(data);
                return;
            }
            Node temp = _root;
            while (true)
            {
                if(data<=temp.Data)
                {
                    if (temp.LeftNode == null)
                    {
                        temp.LeftNode = new Node(data);
                        break;
                    }
                    else
                    {
                        temp = temp.LeftNode;
                    }
                }
                else
                {
                    if (temp.RightNode == null)
                    {
                        temp.RightNode = new Node(data);
                        break;
                    }
                    else
                    {
                        temp = temp.RightNode;
                    }
                }
            }
            
        }
        
        bool Search(int data)
        {
            Node temp = _root;
            while (temp != null)
            {
                if(data == temp.Data)
                {
                    return true;
                }
                else
                {
                    if (data < temp.Data)
                    {
                        temp = temp.LeftNode;
                    }
                    else
                    {
                        temp = temp.RightNode;
                    }
                }
                
            }
            return false;
        }

        void PrintAscending()
        {
            PrintAscending(_root);
            Console.WriteLine();
        }

        private void PrintAscending(Node temp)
        {
            if (temp == null)
            {
                return;
            }
            PrintAscending(temp.LeftNode);
            Console.Write(temp.Data + " ");
            PrintAscending(temp.RightNode);
        }

        void PrintDescending()
        {
            PrintDescending(_root);
            Console.WriteLine();
        }

        private void PrintDescending(Node temp)
        {
            if (temp == null)
            {
                return;
            }
            PrintDescending(temp.RightNode);
            Console.Write(temp.Data + " ");
            PrintDescending(temp.LeftNode);
        }

        static void Main(string[] args)
        {
            BSTree tree = new BSTree();
            Random random = new Random();
            int numOfNodes = int.Parse("" + Console.ReadLine());
            for (int i = 0; i < numOfNodes; i++)
            {
                tree.Insert(int.Parse(Console.ReadLine()));
            }
            Console.WriteLine("Enter 1 : Ascending Order \n 2: Descending Order \n");
            int choice = int.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1 : tree.PrintAscending();
                    break;
                case 2 : tree.PrintDescending();
                    break;
                default : Console.WriteLine("Wrong Input");
                    break;
            }
            Console.ReadLine();

        }
    }
}
