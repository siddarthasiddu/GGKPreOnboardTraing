﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Stack
{
    class Stack<T>
    {
        Node<T> _head = null;
        int _size = 0;
        StringBuilder _str = new StringBuilder("[]");
        public int size
        {
            get
            {
                return _size;
            }
        }
        public void push(T data)
        {            
                if (_head == null) { 
                    _head = new Node<T>(data);
                    _str.Insert(1, data + "");
            }
            else
            {
                Node<T> temp = new Node<T>(data);
                temp.next = _head;
                _head = temp;
                _str.Insert(1, data + ",");
            }
            _size++;
        }
        public T pop()
        {
            if (_head == null)
            {
                throw new Exception("Empty Stack");
            }
            Node<T> temp = _head;
            _head = _head.next;
            temp.next = null;
            int len = ("" + temp.data).Length;
            _str.Remove(1, len + 1);
            return temp.data;
        }
     //   override
        override public string ToString()
        {
            return _str.ToString();
        }
    }
}
