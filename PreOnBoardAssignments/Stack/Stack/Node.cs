﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack
{
    class Node<T>
    {
        T _data;
        Node<T> _next;
        public Node(T data) {
            this.data = data;
        }
        public T data
        {
            get
            {
                return _data;
            }
            set
            {
                _data = value;
            }
        }
        public Node<T> next
        {
            get
            {
                return _next;
            }
            set
            {
                _next = value;
            }
        }
        

    }
}
