﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<int> stack = new Stack<int>();
            Console.WriteLine("Enter number of queries");
            string input = Console.ReadLine();
            Console.WriteLine("Enter 1 for push \nEnter 2 for pop\nEnter 3 for current size of stack\nEnter 4 to print stack\n");
            int n = int.Parse(input);
            for(int i = 0; i <n; i++)
            {
                string[] arr = Console.ReadLine().Split(' ') ;
                switch (int.Parse(arr[0]))
                {
                    case 1: stack.push(int.Parse(arr[1])); break;
                    case 2: Console.WriteLine("poped = " + stack.pop());break;
                    case 3: Console.WriteLine("size = " + stack.size);break;
                    case 4: Console.WriteLine(stack); break;
                    default: Console.WriteLine("you entered wrong choice");
                        break;
                }
            }
            Console.ReadKey();

 
        }
    }
}
