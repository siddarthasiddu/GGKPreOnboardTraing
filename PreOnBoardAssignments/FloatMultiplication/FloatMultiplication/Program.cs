﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloatMultiplication
{
    
    
    class Program
    {
        
        public static void Main(string[] args)
        {
        String[] input = Console.ReadLine().Split(' ');
        float number1 = float.Parse(input[0]);
        float number2 = float.Parse(input[1]);
        FloatMultiplier floatMultiplier = new FloatMultiplier();
        var result = floatMultiplier.Multiply(number1, number2);
        Console.WriteLine(result);
        Console.ReadKey();
        }
    }
}
