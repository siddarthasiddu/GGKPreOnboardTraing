﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FloatMultiplication;
using System.Collections;
namespace FloatMultiplication
{
    public class FloatMultiplier
    {
        public float Multiply(float number1,float number2)
        {
            string BinaryNumber1 = Binary.FromFLoat(number1);
            string BinaryNumber2 = Binary.FromFLoat(number2);
            float value = 0;
            ArrayList intermediateSum = new ArrayList();
            int decimalPointPosition = BinaryNumber1.IndexOf('.') + BinaryNumber2.IndexOf('.');
            BinaryNumber1 =BinaryNumber1.Replace(".","");
            BinaryNumber2=BinaryNumber2.Replace(".", "");
            int BinaryNumber1Length = BinaryNumber1.Length;
            int BinaryNumber2Length = BinaryNumber2.Length;
            int intermediateSumLength = BinaryNumber1Length + BinaryNumber2Length;
            for (int i = 0; i < intermediateSumLength; i++)
            {
                intermediateSum.Add(0);
            }
            for (int i = 0; i < BinaryNumber2Length; i++)
            {
                int pos = intermediateSumLength-1-i;
                for (int j = BinaryNumber1Length - 1; j > -1; j--)
                {
                    if (BinaryNumber2[BinaryNumber2Length-1-i] == '1' && BinaryNumber1[j] == '1')
                    {
                        if (intermediateSum[pos].Equals(1))
                        {
                            for (int k = pos ; k > -1; k--)
                            {
                                if (intermediateSum[k].Equals(1))
                                {
                                    intermediateSum[k] = 0;
                                    
                                }
                                else
                                {
                                    intermediateSum[k] = 1;
                                    break;
                                    
                                }
                            }
                        }
                        else
                        {
                            intermediateSum[pos] = 1;
                        }                        
                    }
                    pos--;
                }
            }
            value = Binary.ToFloat(intermediateSum, decimalPointPosition);
            return value;
        }
    }
}