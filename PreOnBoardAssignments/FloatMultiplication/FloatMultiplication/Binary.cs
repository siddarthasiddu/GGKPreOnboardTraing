using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;


namespace FloatMultiplication
{
    public class Binary
    {
        public static string FromFLoat(float value)
        {
            int decimalValue = (int)value;
            float fractionalValue = value - decimalValue;
            return FromInt(decimalValue)+"."+FromFraction(fractionalValue);

        }
        public static string FromInt(int value)
        {
            StringBuilder binary = new StringBuilder("");
            while (value != 0)
            {
                int remainder = value % 2;
                value /= 2;
                binary.Insert(0,remainder);
            }
            return binary.ToString();
        }
        static string FromFraction(float value)
        {
            StringBuilder binary = new StringBuilder("");
            int maxLength = 1000;
            for(int i = 0; i < maxLength && value!=0; i++)
            {
                
                int remainder = (int)(value * 2);
                value = (value * 2) - remainder;
                binary.Append(remainder);
            }
            binary.Append(0);
            return binary.ToString();
        }
        public static float ToFloat(ArrayList binary,int decimalPointPosition)
        {
            StringBuilder decimalBinary= new StringBuilder("");
            StringBuilder fractionBinary = new StringBuilder("");
            for(int i = 0; i < binary.Count; i++)
            {
                if (i < decimalPointPosition)
                {
                    decimalBinary.Append(binary[i]);
                }
                else
                {
                    fractionBinary.Append(binary[i]);
                }
            }
            return ToInt(decimalBinary)+ToFraction(fractionBinary);
        }
        public static int ToInt(StringBuilder decimalBinary)
        {
            int value = 0;
            for(int i = decimalBinary.Length - 1; i > -1; i--)
            {
                if (decimalBinary[i] == '1')
                {
                    value = value + ((int)Math.Pow(2, decimalBinary.Length -1 - i));
                }
            }
            return value;
        }
        private static float ToFraction(StringBuilder fractionBinary)
        {
            double value = 0;
            for (int i = 0; i < fractionBinary.Length; i++)
            {
                if (fractionBinary[i] == '1')
                {
                    value = value + (Math.Pow(2,-i-1));
                }
            }
            return (float)value;
        }
        

    }
}