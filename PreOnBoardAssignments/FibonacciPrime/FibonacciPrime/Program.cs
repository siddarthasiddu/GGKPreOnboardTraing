﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciPrime
{
    class Program
    {
        static void Main(string[] args)
        {
            int required = int.Parse(Console.ReadLine());
            FibonacciPrimeGenerator fibonacciPrimeGenerator= new FibonacciPrimeGenerator();
            Console.Write(fibonacciPrimeGenerator.Get(required));
            Console.Read();

        }
    }
}
