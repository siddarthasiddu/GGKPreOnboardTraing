﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciPrime
{
    internal class FibonacciPrimeGenerator
    {
        internal string Get(int count) 
        {
            string result = "";
            string intermediateResult = "";
            int previousNumber = 1, fibonacci = 2;
            while(count>1)
            {
                int divisors = 0;
                for(int i = 1; i <= (int)Math.Sqrt(fibonacci); i++)
                {
                    if (fibonacci % i == 0)
                    {
                        divisors++;
                    }
                    if (divisors > 1) break;
                }
                if (divisors == 1)
                {
                    
                    intermediateResult = intermediateResult + fibonacci + " ";
                    result =result+ intermediateResult +"\n";
                    
                    
                }
                fibonacci = fibonacci + previousNumber;
                previousNumber = fibonacci - previousNumber;
                count--;
                
            }
            return result.Equals("")?"Empty":result;
        }
    }
}