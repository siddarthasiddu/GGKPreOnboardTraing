﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorDesign
{
    abstract class IVehicle
    {
        public abstract void Build();
    }
}
