﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorDesign
{
    class Program
    {
        static void Main(string[] args)
        {
            Factory factory = new Factory();
            IVehicle bus = factory.BuildVehicle("bus");
            bus.Build();
            IVehicle car = factory.BuildVehicle("car");
            car.Build();
            IVehicle truck = factory.BuildVehicle("truck");
            truck.Build();
            Console.ReadKey();
        }
    }
}
