﻿using System;

namespace FactorDesign
{
    class Bus : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Bus is building");
        }
    }
}
