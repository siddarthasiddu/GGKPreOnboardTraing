﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorDesign
{
    class Car : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Car is building");
        }
    }
}
