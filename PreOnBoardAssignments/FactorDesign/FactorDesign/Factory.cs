﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorDesign
{
    class Factory
    {
        public IVehicle BuildVehicle(String vehicleType)
        {
            if (vehicleType == null)
            {
                return null;
            }
            if (vehicleType.ToLower().Equals("bus"))
            {
                return new Bus();

            }
            if (vehicleType.ToLower().Equals("car"))
            {
                return new Car();

            }
            if (vehicleType.ToLower().Equals("truck"))
            {
                return new Truck();
            }

            return null;
        }
    }
}
