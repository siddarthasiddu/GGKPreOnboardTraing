﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorDesign
{
    class Truck : IVehicle
    {
        public override void Build()
        {
            Console.WriteLine("Truck is building");
        }
    }
}
