﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigSum
{
    class BigSum
    {
        string Add(StringBuilder num1,StringBuilder num2)
        {
            bool carry = false;
            if(num1.Length < num2.Length)
            {
                StringBuilder temp = num2;
                num2 = num1;
                num1 = temp;
            }
            int num1Pos = num1.Length - 1;
            int num2Pos = num2.Length - 1;
            for (; num2Pos > -1; num2Pos--,num1Pos--)
            {
                int val1 = int.Parse("" + num1[num1Pos]);
                int val2 = int.Parse("" + num2[num2Pos]);
                if (carry == true)
                {
                    val1++;
                    carry = false;
                }
                if (val1 + val2 > 9)
                {
                    carry = true;
                }
                char val = (char)(((int)'0') + ((val1 + val2) % 10));
                num1[num1Pos] = (val);
                
            }
            if (carry == true)
            {
                if (num1.Length == num2.Length)
                {
                    num1.Insert(0,""+1);
                    carry = false;
                }
                while (carry == true && num1Pos>-1) 
                {
                   int val = int.Parse("" + num1[num1Pos]);
                    val++;
                    if (val < 10)
                    {
                        carry = false;
                    }  
                    num1[num1Pos--]= (char)(((int)'0') + ((val) % 10));
                }
                if (carry == true)
                {
                    num1.Insert(0, 1);
                    carry = false;
                }
            }
            return num1.ToString();
        }
        static void Main(string[] args)
        {
            StringBuilder num1 = new StringBuilder(Console.ReadLine());
            StringBuilder num2 = new StringBuilder(Console.ReadLine());
            BigSum bigSum = new BigSum();
            string sum = bigSum.Add(num1, num2);
            Console.WriteLine(sum);
            Console.Read();
        }
    }
}
