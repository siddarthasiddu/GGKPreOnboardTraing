﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringConversion
{
    class Program
    {   
        bool isPrime(int val)
        {
            int n = 130;
            bool[] primes=null;
            if (primes != null)
            {
                return primes[val];
            }
            else
            {
                primes = new bool[n+1];
                for (int i = 0; i < n; i++)
                {
                    primes[i] = true;
                }
                for (int i = 2; (i * i) <= n; i++)
                {
                    if (primes[i] == true)
                    {
                        for (int j = i * 2; j <= n; j = j + i)
                        {
                            primes[j] = false;
                        }
                    }

                }
                return primes[val];
            }
        }
        String ConvertString(String str)
        {
            char[] charArray = new char[str.Length - 1];
            for(int i = 0; i < charArray.Length; i++)
            {
                charArray[i] = (char)((int)str[i] + (int)str[i+1]);
                charArray[i] =(char)((int)charArray[i]/2);
                if (isPrime((int)charArray[i]))
                {
                    charArray[i]++;
                }
            }
            return new String(charArray);
        }
        static void Main(string[] args)
        {
            Program test = new Program();
            String str = Console.ReadLine();
            Console.Write(test.ConvertString(str));
            Console.ReadLine();
        }
    }
}
