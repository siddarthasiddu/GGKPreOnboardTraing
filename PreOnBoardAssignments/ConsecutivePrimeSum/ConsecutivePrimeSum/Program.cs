﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsecutivePrimeSum
{
    class ConsecutivePrimeSum
    {
        bool[] primes;
        void GeneratePrime(int n)
        {
            primes = new bool[n+1];
            for(int i = 0; i < n; i++)
            {
                primes[i] = true;
            }
            for(int i = 2;( i * i) <= n; i++)
            {
                if (primes[i] == true)
                {
                    for(int j = i * 2; j <= n; j = j + i)
                    {
                        primes[j] = false;
                    }
                }
            }    
        }

        bool isPrime(int n)
        {
            if (n > primes.Length - 1)
            {
                return false;
            }
            return primes[n];
        }
        
        int ConsecutiveSumPrimeCount(int n)
        {
            GeneratePrime(n);
            int sum = 2;
            int count = 0;
            for(int i = 3; i < n; i++)
            {
                if (isPrime(i))
                {
                    sum = sum + i;
                    if (sum <= n)
                    {
                        if (isPrime(sum))
                        {
                            count++;
                            
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return count;
        }

        static void Main(string[] args)
        {
            ConsecutivePrimeSum test = new ConsecutivePrimeSum();
            int n = int.Parse(Console.ReadLine());
            Console.Write(test.ConsecutiveSumPrimeCount(n));
            Console.ReadLine();
            Console.Write("ok");
            Console.Read();
        }
    }
}
