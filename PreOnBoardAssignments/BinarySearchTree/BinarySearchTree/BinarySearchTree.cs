﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BinarySearchTree
{
    class BinarySearchTree<T> where T : IComparable
    {
        Node<T> root = null;
        int _size = 0;
        public int size
        {
            get
            {
                return _size;
            }
        }
        public void Insert(T data)
        {
            _size++;
            if (root == null)
            {
                root = new Node<T>(data);
                return;
            }
            Node<T> current = root;
            while (true)
            {
                if (data.CompareTo(current.data)<0)
                {
                    if (current.left == null)
                    {
                        current.left=new Node<T>(data);
                        return;
                    }
                    current = current.left;
                }
                else
                {
                    if (current.right == null)
                    {
                        current.right = new Node<T>(data);
                        return;
                    }
                    current= current.right;
                }
            }
        }
        public void PrintInOrder()
        {
            PrintInOrder(root);
        }
        void PrintInOrder(Node<T> root)
        {
            if (root == null) return;
            PrintInOrder(root.left);
            Console.Write(root.data + " ");
            PrintInOrder(root.right);
        }
        
        

    }
}
