﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    
        class Node<T>  where T : IComparable
        {
            T _data;
            Node<T> _left;
            Node<T> _right;
            public Node(T data)
            {
                this.data = data;
            }
            public int CompareTo( object obj)
            {
                return this.CompareTo(obj);
            }
            public T data
            {
                get
                {
                    return _data;
                }
                set
                {
                    _data = value;
                }
            }
            public Node<T> left
            {
                get
                {
                    return _left;
                }
                set
                {
                    _left = value;
                }
            }
            public Node<T> right
            {
                get
                {
                    return _right;
                }
                set
                {
                    _right = value;
                }
            }


        }
    }

