﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarySearchTree
{
    class Program
    {
        static void Main(string[] args)
        {
            BinarySearchTree<int> bst = new BinarySearchTree<int>();
            for(int i = 0; i < 10; i++)
            {
                bst.Insert(new Random().Next());
            }
            bst.PrintInOrder();

        }
    }
}
