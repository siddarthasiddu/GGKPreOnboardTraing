﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph
{
     class MinimumHeap
    {
        private int[] _arr;
        private int _size;
        private int _count;
        private int[] _Weights;
        private int _WeightsSize;
        internal MinimumHeap(int _size,int[] distances)
        {
            _arr = new int[_size + 1];
            this._size = _size + 1;
            _count = 0;
            _Weights = distances;
            _WeightsSize = distances.Length;
        }
        
        internal int Capacity()
        {
            return _size - 1;
        }

        internal int size()
        {
            return _count;
        }

        internal bool IsEmpty()
        {
            return _count == 0;
        }

        internal int Peek()
        {
            return _arr[1];
        }
        
        internal int Poll()
        {
           
            int value = _arr[1];
            _arr[1] = _arr[_count];
            _count--;
            ReArrange();
            return value;
        }

        internal void Add(int a)
        {

            if (_count == _size - 1)
            {
                NewArray(a);
            }
            else
            {
                _arr[_count + 1] = a;
                _count++;
            }
            ReArrange();
        }

        private void ReArrange()
        {
            int pos = _count / 2;
            for (int i = pos; i > 0; i--)
            {
                if ((2 * i ) > _size || (2 * i ) > _count)
                {
                    if (_Weights[_arr[i]-1] > _Weights[_arr[2 * i]-1])
                    {
                        int temp = _arr[i];
                        _arr[i] = _arr[2 * i];
                        _arr[2 * i] = temp;
                    }
                }
                else
                {
                    int min = -1;
                    if (_Weights[_arr[2 * i]-1] < _Weights[_arr[2 * i + 1]-1]) min = 2 * i;
                    else min = 2 * i + 1;
                    if (_Weights[_arr[i]-1] > _Weights[_arr[min]-1])
                    {
                        int temp = _arr[i];
                        _arr[i] = _arr[min];
                        _arr[min] = temp;
                    }
                }
            }

        }
        
        internal bool Contains(int data)
        {
            for(int i = 1; i <= _count; i++)
            {
                if (_arr[i] == data) return true;
            }
            return false;
        }
        internal int Remove(int data)
        {
            int temp;
            int x = _arr[1];
            for (int i = 1; i <= _count; i++)
            {
                if (_arr[i] == data && i != _count)
                {
                    temp = _arr[i];
                    _arr[i] = _arr[_count];
                    _count--; break;
                }
                if (i == _count && _arr[i] == data)
                {
                    temp = _arr[i];
                    _count--; break;
                }
            }
             ReArrange();
            return _count;
        }

        

        private void NewArray(int a)
        {
            int[] temp = new int[_size * 2];
            for (int i = 0; i < _size; i++)
            {
                temp[i] = _arr[i];
            }
            _arr = temp;
            temp[_count + 1] = a;
            _count++;
            _size = _size * 2;
        }

        
    }
}
