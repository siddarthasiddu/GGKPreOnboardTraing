﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph
{
 
   
   class Graph
    {
        List<Dictionary<int,int>> _list = new List<Dictionary<int, int>>();
        int NodeCount = 0;
        Graph(int nodes)
        {
           for(int i = 0; i < nodes; i++)
            {
                _list.Add(new Dictionary<int, int>());
            }
            NodeCount = nodes;
        }

        void AddEdge(int from , int to , int weight)
        {
            from--;
            to--;
            _list.ElementAt(from).Add(to, weight);
            _list.ElementAt(to).Add(from, weight);
        }

        Dictionary<int,int> AdjVertices(int from)
        {
           return _list.ElementAt(from);
        }

        void PrintGraph()
        {
            for(int i = 0; i < NodeCount; i++)
            {
                Console.Write(i + 1 + "   -> ");
                foreach(int to in _list.ElementAt(i).Keys){
                    int weight=0;
                    _list.ElementAt(i).TryGetValue(to,out weight);
                    Console.Write(to + 1 + ":" + weight+"  |  ");
                }
                Console.WriteLine();
            }
        }

        void ShortestPath(int source, int destination)
        {
            source--;
            destination--;
            Dictionary<int,int>  previous = new Dictionary<int, int>();
            Dictionary<int,int> distances = new Dictionary<int, int>();
            List<int> nodes = new List<int>();
            List<int> path = null;
            for(int i = 0; i < _list.Count; i++)
            {
                if 
                    (i == source) distances[i] = 0;
                else
                    distances[i] = int.MaxValue;
                nodes.Add(i);
            }
            
            while (nodes.Count != 0)
            {
                nodes.Sort((a, b) => distances[a] - distances[b]);
                int smallest = nodes[0];
                nodes.Remove(smallest);
                if (smallest == destination)
                {
                    path = new List<int>();
                    while (previous.ContainsKey(smallest))
                    {
                        path.Add(smallest+1);
                        smallest = previous[smallest];
                    }
                    break;
                }
                if (distances[smallest] == int.MaxValue)
                {
                    break;
                }
                foreach(KeyValuePair<int,int> neighbour in _list[smallest])
                {
                    int newDistance = distances[smallest] + neighbour.Value;
                    if (newDistance < distances[neighbour.Key])
                    {
                        distances[neighbour.Key] = newDistance;
                        previous[neighbour.Key] = smallest;
                    }
                }
            }
            path.Add(source+1);
            path.Reverse();
            Console.Write("The Shortest path is ");
            for (int i = 0; i < path.Count; i++)
            {
                
                if (i == 0)
                {
                    Console.Write(path.ElementAt(i));
                }
                else
                {
                    Console.Write("->" + path.ElementAt(i));
                }

            }
            Console.WriteLine(" with total distance of "+distances[destination]);
            
        }


        void Djikstra(int source,int destination)
        {
            source--;
            destination--;
            int[] distances = new int[NodeCount];
            int[] previous = new int[NodeCount];
            List<int> visited = new List<int>();
            List<int> unvisited = new List<int>();
            for(int i = 0; i < NodeCount; i++)
            {
                distances[i] = int.MaxValue;
                if (i == source)
                {
                    distances[i] = 0;
                }
                unvisited.Add(i);
            }
            while (visited.Count()!=NodeCount)
            {
                unvisited.Sort((int a, int b) => distances[a] - distances[b]);
                int vertex = unvisited.ElementAt(0);
                unvisited.Remove(vertex);
                foreach(KeyValuePair<int,int> neighbourNode in AdjVertices(vertex))
                {
                    if (unvisited.Contains(neighbourNode.Key))
                    {
                        int newDistance = distances[vertex] + neighbourNode.Value;
                        if (newDistance < distances[neighbourNode.Key])
                        {
                            distances[neighbourNode.Key] = newDistance;
                            previous[neighbourNode.Key] = vertex;
                        }
                    }
                    
                }
                visited.Add(vertex);

            }
            List<int> path = new List<int>();
            int current = destination;
            while (current != source)
            {
                path.Add(current + 1);
                current = previous[current];
            }
            path.Add(current + 1);
            path.Reverse();
            Console.Write("The Shortest path is ");
            for (int i = 0; i < path.Count; i++)
            {
                if (i == 0)
                {
                    Console.Write(path.ElementAt(i));
                }
                else
                {
                    Console.Write("->" + path.ElementAt(i));
                }

            }
            Console.WriteLine(" with total distance of " + distances[destination]);
            
        }

        void Djikestra(int source, int destination)
        {
            source--;
            destination--;
            int[] distances = new int[NodeCount];
            int[] previous = new int[NodeCount];
            List<int> visited = new List<int>();
            //List<int> unvisited = new List<int>();
            MinimumHeap unvisited = new MinimumHeap(10+NodeCount,distances);
            for (int i = 0; i < NodeCount; i++)
            {
                distances[i] = int.MaxValue;
                if (i == source)
                {
                    distances[i] = 0;
                }
                unvisited.Add(i);
            }
            while (visited.Count() != NodeCount)
            {
                
                int vertex = unvisited.Poll();
                foreach (KeyValuePair<int, int> neighbourNode in AdjVertices(vertex))
                {
                    if (unvisited.Contains(neighbourNode.Key))
                    {
                        int newDistance = distances[vertex] + neighbourNode.Value;
                        if (newDistance < distances[neighbourNode.Key])
                        {
                            distances[neighbourNode.Key] = newDistance;
                            previous[neighbourNode.Key] = vertex;
                        }
                    }

                }
                visited.Add(vertex);

            }
            List<int> path = new List<int>();
            int current = destination;
            while (current != source)
            {
                path.Add(current + 1);
                current = previous[current];
            }
            path.Add(current + 1);
            path.Reverse();
            Console.Write("The Shortest path is ");
            for (int i = 0; i < path.Count; i++)
            {
                if (i == 0)
                {
                    Console.Write(path.ElementAt(i));
                }
                else
                {
                    Console.Write("->" + path.ElementAt(i));
                }

            }
            Console.WriteLine(" with total distance of " + distances[destination]);
            Console.Read();
        }

        static void Main(string[] args)
        {
            Console.Write("enter number of nodes : ");
            int nodeCount = int.Parse(""+Console.ReadLine());
            Console.Write("enter number of Edges : ");
            int edgeCount = int.Parse("" + Console.ReadLine());
            Graph graph = new Graph(nodeCount);
            for (int i = 0; i < edgeCount; i++)
            {
                int node1, node2, weight;
                string[] values = Console.ReadLine().Split();
                node1 = int.Parse(values[0]);
                node2 = int.Parse(values[1]); 
                weight = int.Parse(values[2]);
                graph.AddEdge(node1, node2, weight);
            }
          //  graph.PrintGraph();
            Console.WriteLine("enter Source Node = ");
            int source = int.Parse(Console.ReadLine());
            Console.WriteLine("enter Destination Node = ");
            int destination = int.Parse(Console.ReadLine());
            graph.ShortestPath(source, destination);
            Console.WriteLine();
            graph.Djikstra(source, destination);
            Console.WriteLine();
            graph.Djikestra(source, destination);
            Console.Read();
            Console.Read();
            
            
        }
    }
}
