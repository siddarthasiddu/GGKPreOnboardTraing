﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloatMultiplication
{
    class Multiplier
    {
        internal Program.FloatNumber FloatMultiplication(Program.FloatNumber firstInput,Program.FloatNumber secondInput)
        {
            BinaryConverter binaryconverter = new BinaryConverter();
            String firstInputBinary = binaryconverter.FloatToBinary(firstInput.number);
            String secondInputBinary = binaryconverter.FloatToBinary(secondInput.number);
            float value = 0;
            ArrayList intermediateSum = new ArrayList();
            int decimalPointPosition = firstInputBinary.IndexOf('.') + secondInputBinary.IndexOf('.');
            firstInputBinary = firstInputBinary.Replace(".", "");
            secondInputBinary = secondInputBinary.Replace(".", "");
            int BinaryNumber1Length = firstInputBinary.Length;
            int BinaryNumber2Length = secondInputBinary.Length;
            int intermediateSumLength = BinaryNumber1Length + BinaryNumber2Length;
            for (int i = 0; i < intermediateSumLength; i++)
            {
                intermediateSum.Add(0);
            }
            for (int i = 0; i < BinaryNumber2Length; i++)
            {
                int pos = intermediateSumLength - 1 - i;
                for (int j = BinaryNumber1Length - 1; j > -1; j--)
                {
                    if (secondInputBinary[BinaryNumber2Length - 1 - i] == '1' && firstInputBinary[j] == '1')
                    {
                        if (intermediateSum[pos].Equals(1))
                        {
                            for (int k = pos; k > -1; k--)
                            {
                                if (intermediateSum[k].Equals(1))
                                {
                                    intermediateSum[k] = 0;

                                }
                                else
                                {
                                    intermediateSum[k] = 1;
                                    break;

                                }
                            }
                        }
                        else
                        {
                            intermediateSum[pos] = 1;
                        }
                    }
                    pos--;
                }
            }
            value = binaryconverter.BinaryToFloat(intermediateSum, decimalPointPosition);
            Program.FloatNumber result = new Program.FloatNumber(value);
            if ((firstInput.isPositive==false && secondInput.isPositive==true) || (firstInput.isPositive == true && secondInput.isPositive == false) )
            {
               // Console.WriteLine("ok" + "    " + value);
                //value = Math.Abs(value);
               
                result.number = -value;
            }
           // Console.WriteLine("sdf" + result.number);
            return result;


            
        }
    }
}
