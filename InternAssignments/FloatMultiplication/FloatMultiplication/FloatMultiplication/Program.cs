﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FloatMultiplication
{
   class Program
    {
        internal class FloatNumber
        {
            internal float number;
            
            internal Boolean isPositive;
            internal FloatNumber(float value)
            {
                number = value;
                if (value >= 0)
                {
                    isPositive = true;
                }
                else
                {
                    number = Math.Abs(number);
                    isPositive = false;
                }

            }
            public static FloatNumber operator *(FloatNumber firstInput, FloatNumber secondInput)
            {
                Multiplier multiplier = new Multiplier();
                FloatNumber result = multiplier.FloatMultiplication(firstInput, secondInput);
                
                return result;
            }
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine("Enter two Numbers to multiply");
            string[] input = Console.ReadLine().Split(' ');
            Program p = new FloatMultiplication.Program();
            FloatNumber firstInput = new FloatNumber(float.Parse(input[0]));
            FloatNumber secondInput = new FloatNumber(float.Parse(input[1]));
            float result = (firstInput * secondInput).number;
            Console.WriteLine(result);
          //  Console.WriteLine(new BinaryConverter().BinaryToFloat(new StringBuilder("101."));
            Console.ReadKey();
        }
    }
}
